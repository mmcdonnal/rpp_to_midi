import os
from optparse import OptionParser

def variable_len_value(track_data, value):
	if value <= 0x7f:
		track_data.append(value)
	elif value <= 0x6FFF:
		high = (value & 0x3F80)
		byte1 = 0x80 | (high >> 7)
		byte2 = (value & 0x007F)
		track_data.append(byte1)
		track_data.append(byte2)
	elif value <= 0x4FFFFF:
		high = value & 0x1FC000
		byte1 = 0x80 | (high >> 14)
		high = value & 0x003F80
		byte2 = 0x80 | (high >> 7)
		byte3 = value & 0x007F
		track_data.append(byte1)
		track_data.append(byte2)
		track_data.append(byte3)
	else:
		high = value & 0xFE00000
		byte1 = 0x80 | (high >> 21)
		high = value & 0x1FC000
		byte2 = 0x80 | (high >> 14)
		high = value & 0x003F80
		byte3 = 0x80 | (high >> 7)
		byte4 = value & 0x007F
		track_data.append(byte1)
		track_data.append(byte2)
		track_data.append(byte3)
		track_data.append(byte4)

def process_one_file(filename, outdir, native):
	with open(filename) as f:
		item_names = {}
		base_name = os.path.splitext(filename)[0]
		in_item = False
		item_name = None
		in_midi = False
		for line in f:
			if '<ITEM' in line:
				in_item = True
			elif in_item and 'NAME' in line:
				item_name = line[12:-2]
			elif in_item and item_name and '<SOURCE MIDI' in line:
				in_midi = True
				if item_name in item_names:
					item_names[item_name] += 1
				else:
					item_names[item_name] = 0
				if native:
					midi_filename = '%s/%s_%s_%d.reamid' % (outdir, base_name, item_name, item_names[item_name])
					outfile = open(midi_filename, 'w')
				else:
					midi_filename = '%s/%s_%s_%d.mid' % (outdir, base_name, item_name, item_names[item_name])
					outfile = open(midi_filename, 'wb')
					header = bytearray(['M','T','h','d',0,0,0,6,0,0,0,1,0x03,0xc0,'M','T','r','k'])
					track_data = []
					outfile.write(header)
			elif in_midi:
				if '>' in line:
					if native:
						outfile.close()
					else:
						total_len = len(track_data) + 4
						len3 = total_len & 0x000000ff
						len2 = (total_len & 0x0000ff00) >> 8
						len1 = (total_len & 0x00ff0000) >> 16
						len0 = (total_len & 0xff000000) >> 24
						outfile.write(bytearray([len0, len1, len2, len3]))
						outfile.write(bytearray(track_data))
						outfile.write(bytearray([0,0xff,0x2f,0]))
						outfile.close()
					in_midi = False
					in_item = False
					item_name = None
				else:
					if native:
						outfile.write(line)
					else:
						if line[0:10] == '        E ':
							midi_data = line[10:-1].split(' ')
							variable_len = variable_len_value(track_data, int(midi_data[0]))
							track_data.append(int(midi_data[1], 16))
							track_data.append(int(midi_data[2], 16))
							track_data.append(int(midi_data[3], 16))

def main():
    parser = OptionParser(
            """%prog 
            Converts the MIDI sections of a Reaper RPP file
            to MIDI standard files.
            """)
    pao = parser.add_option
    pao("--file", action="store", help="RPP file to extract from.")
    pao("--out", action="store", help="Output directory.")
    pao("--native", action="store_true", help="Write files in Reaper format rather than MIDI standard format.")

    (options, args) = parser.parse_args()

    process_one_file(options.file, options.out, options.native)
    
if __name__ == '__main__':
    main()