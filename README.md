# rpp_to_midi #



Converts the MIDI sections of a Reaper RPP file to MIDI standard files.

Usage: python rpp_to_midi.py 

Options:
```
--file=FILE  RPP file to extract from.
--out=OUT    Output directory.
--native     Write files in Reaper format rather than MIDI standard format.
```